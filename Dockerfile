FROM python:3.7-buster

COPY main.py .

RUN mkdir data
RUN mkdir models

COPY data/data-set.csv data/

COPY requirements.txt .
RUN pip install -r requirements.txt

CMD ["python", "main.py"]
