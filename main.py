import pandas as pd
import numpy as np
import xgboost as xgb
from sklearn.model_selection import StratifiedShuffleSplit
from sklearn import preprocessing
from sklearn.metrics import log_loss, accuracy_score, recall_score


data = pd.read_csv("data/data-set.csv")
data["lastVisit"] = pd.to_datetime(data["lastVisit"])
data["product02"] = list(map(lambda x: 0 if x == "Nee" else 1, data["product02"]))

le = preprocessing.LabelEncoder()
for column in ["gender", "house_type"]:
    le.fit(data[column])
    data[column] = le.transform(data[column])

data["lastVisitHour"] = list(map(lambda x: x.hour, data["lastVisit"]))

X = data.copy()
del X["product02"], X["lastVisit"], X["subscriber"]
X = np.array(X.values)

y = pd.DataFrame(data["product02"].copy())
y = np.array(y.values)

sss = StratifiedShuffleSplit(n_splits=5, test_size=0.5)

fold = 1
logloss_scores = []
for train_index, test_index in sss.split(X, y):
    X_train, X_test = X[train_index], X[test_index]
    y_train, y_test = y[train_index], y[test_index]
    xgb_c = xgb.XGBClassifier(objective='binary:logistic',eval_metric="logloss", n_estimators=100, use_label_encoder=False)
    xgb_c.fit(X_train, y_train)
    y_pred = xgb_c.predict(X_test)
    score = accuracy_score(y_test, y_pred)
    logloss_scores.append(score)    
    fold+=1
    print(f"[Fold {fold}]   accuracy {score:.3f}, recall {recall_score(y_test, y_pred):.3f}, logloss {log_loss(y_test, y_pred):.3f}")

print(f"Mean acc {np.mean(logloss_scores):.3f}")

xgb_c = xgb.XGBClassifier(objective='binary:logistic',eval_metric="logloss", n_estimators=100, use_label_encoder=False)
xgb_c.fit(X, y)
xgb_c.save_model('models/simple-model.model')

print("Done.")